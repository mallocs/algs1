/* *****************************************************************************
 *  Name: Marcus Ulrich
 **************************************************************************** */

import edu.princeton.cs.algs4.Picture;
import edu.princeton.cs.algs4.StdOut;

public class SeamCarver {
    private Picture picture;
    private double[][] distTo;
    private int oldestCacheIndex;
    private int[][] energyCacheKey;
    private double[] energy;
    private int[][] edgeTo;
    private boolean isTransposed = false;

    // create a seam carver object based on the given picture
    public SeamCarver(Picture picture) {
        if (picture == null) throw new IllegalArgumentException("argument is null");
        this.picture = new Picture(picture);
        int maxDimension = picture.width() > picture.height() ? picture.width() : picture.height();
        distTo = new double[2][maxDimension];
        energyCacheKey = new int[3][2];
        energy = new double[3];
        edgeTo = new int[maxDimension][maxDimension]; // pixel -> col
    }

    private void initDataStructures() {
        int maxDimension = picture.width() > picture.height() ? picture.width() : picture.height();

        for (int i = 0; i < maxDimension; i++) {
            distTo[0][i] = Double.POSITIVE_INFINITY;
            distTo[1][i] = Double.POSITIVE_INFINITY;
            for (int j = 0; j < maxDimension; j++) {
                edgeTo[i][j] = -1;
            }
        }
    }

    // current picture
    public Picture picture() {
        if (isTransposed) {
            transpose();
        }
        return new Picture(picture);
    }

    // width of current picture
    public int width() {
        if (isTransposed) {
            transpose();
        }
        return picture.width();
    }

    // height of current picture
    public int height() {
        if (isTransposed) {
            transpose();
        }
        return picture.height();
    }

    // energy of pixel at column x and row y
    public double energy(int x, int y) {
        return isTransposed ? _energy(y, x) : _energy(x, y);
    }

    private double getEnergyCache(int x, int y) {
        double energyCacheVal = 0.0;
        for (int i = 0; i < 3; i++) {
            if (energyCacheKey[i][0] == x && energyCacheKey[i][1] == y) {
                return energy[i];
            }
        }
        return energyCacheVal;
    }

    private void putEnergyCache(int x, int y, double val) {
        energyCacheKey[oldestCacheIndex] = new int[] {
                x, y
        };
        energy[oldestCacheIndex] = val;
        oldestCacheIndex = (oldestCacheIndex + 1) % 3;
    }

    // energy of pixel at column x and row y
    private double _energy(int x, int y) {
        if (x < 0 || x > picture.width() - 1) {
            throw new IllegalArgumentException("x is out of bounds: " + picture.width() + ", " + x);
        }
        if (y < 0 || y > picture.height() - 1) {
            throw new IllegalArgumentException(
                    "y is out of bounds: " + picture.height() + ", " + y);
        }
        if (x == 0 || y == 0 || x == picture.width() - 1 || y == picture.height() - 1) {
            return 1000.0;
        }
        double energyCacheVal = getEnergyCache(x, y);
        if (energyCacheVal != 0.0) {
            return energyCacheVal;
        }

        int rgbXPlus = picture.getRGB(x + 1, y);
        int rgbXMinus = picture.getRGB(x - 1, y);
        int rgbYPlus = picture.getRGB(x, y + 1);
        int rgbYMinus = picture.getRGB(x, y - 1);

        double energyVal = Math
                .sqrt(deltaSquared(rgbXPlus, rgbXMinus) + deltaSquared(rgbYPlus, rgbYMinus));
        putEnergyCache(x, y, energyVal);
        return energyVal;
    }


    // int r = (rgb >> 16) & 0xFF;
    // int g = (rgb >>  8) & 0xFF;
    // int b = (rgb >>  0) & 0xFF;
    // Given the RGB components (8-bits each) of a color, the following statement packs it into a 32-bit int:
    // int rgb = (r << 16) + (g << 8) + (b << 0);
    private double deltaSquared(int val1, int val2) {
        int r1 = (val1 >> 16) & 0xFF;
        int g1 = (val1 >> 8) & 0xFF;
        int b1 = (val1 >> 0) & 0xFF;
        int r2 = (val2 >> 16) & 0xFF;
        int g2 = (val2 >> 8) & 0xFF;
        int b2 = (val2 >> 0) & 0xFF;
        return (r1 - r2) * (r1 - r2) + (g1 - g2) * (g1 - g2) + (b1 - b2) * (b1 - b2);

    }

    // sequence of indices for vertical seam
    public int[] findVerticalSeam() {
        if (isTransposed) {
            transpose();
        }
        return findSeam();
    }

    // sequence of indices for horizontal seam
    public int[] findHorizontalSeam() {
        if (!isTransposed) {
            transpose();
        }
        return findSeam();
    }

    private int[] findSeam() {
        for (int col = 0; col < picture.width(); col++) {
            distTo[0][col] = _energy(col, 0);
            distTo[1][col] = Double.POSITIVE_INFINITY;
        }
        for (int row = 0; row < picture.height() - 1; row++) {
            for (int col = 0; col < picture.width(); col++) {
                // down, left
                if (col > 0) {
                    relax(row, col, row + 1, col - 1);
                }
                // down
                relax(row, col, row + 1, col);
                // down, right
                if (col < picture.width() - 1) {
                    relax(row, col, row + 1, col + 1);
                }
            }
            for (int col = 0; col < picture.width(); col++) {
                distTo[0][col] = distTo[1][col];
                distTo[1][col] = Double.POSITIVE_INFINITY;
            }
        }
        double finalEnergy = Double.POSITIVE_INFINITY;
        int pathCol = -1;
        for (int col = 0; col < picture.width(); col++) {
            if (finalEnergy > distTo[0][col]) {
                finalEnergy = distTo[0][col];
                pathCol = col;
            }
        }
        int[] path = new int[picture.height()];

        for (int pathRow = picture.height() - 1; pathRow >= 0; pathRow--) {
            path[pathRow] = pathCol;
            pathCol = edgeTo[pathRow][pathCol];
        }
        return path;
    }

    private void relax(int vRow, int vCol, int wRow, int wCol) {
        double energyVal = _energy(vCol, vRow);
        if (distTo[1][wCol] > distTo[0][vCol] + energyVal) {
            distTo[1][wCol] = distTo[0][vCol] + energyVal;
            edgeTo[wRow][wCol] = vCol;
        }
    }

    private void transpose() {
        int width = picture.height();
        int height = picture.width();
        Picture target = new Picture(width, height);

        for (int col = 0; col < width; col++) {
            for (int row = 0; row < height; row++) {
                target.setRGB(col, row, picture.getRGB(row, col));
            }
        }
        picture = target;
        isTransposed = !isTransposed;
        initDataStructures();
    }

    // always removes a vertical seam, so transpose first for horizontal
    private void removeSeam(int[] seam) {
        if (seam == null) throw new IllegalArgumentException("argument is null");
        if (picture.width() <= 1) {
            throw new IllegalArgumentException("Picture is too small to remove seam");
        }
        if (seam.length != picture.height()) {
            throw new IllegalArgumentException("Seam is not the right size for the picture");
        }
        // /target.setRGB(col, row, source.getRGB(row, col));
        Picture target = new Picture(picture.width() - 1, picture.height());
        for (int row = 0; row < picture.height(); row++) {
            int seamCol = seam[row];
            if (seamCol < 0 || seamCol >= picture.width()) {
                throw new IllegalArgumentException("Invalid seam");
            }
            if (row < picture.height() - 1 && Math
                    .abs(seamCol - seam[row + 1]) > 1) {
                throw new IllegalArgumentException("Invalid seam");
            }
            for (int col = 0; col < picture.width() - 1; col++) {
                if (col < seamCol) {
                    target.setRGB(col, row, picture.getRGB(col, row));
                }
                else {
                    target.setRGB(col, row, picture.getRGB(col + 1, row));
                }
            }
        }
        picture = target;
        initDataStructures();
    }

    // remove horizontal seam from current picture
    public void removeHorizontalSeam(int[] seam) {
        if (!isTransposed) {
            transpose();
        }
        removeSeam(seam);
    }


    // remove vertical seam from current picture
    public void removeVerticalSeam(int[] seam) {
        if (isTransposed) {
            transpose();
        }
        removeSeam(seam);
    }

    //  unit testing (optional)
    public static void main(String[] args) {
        Picture picture = new Picture("1x8.png");
        StdOut.printf("%s (%d-by-%d image)\n", "12x10.png", picture.width(), picture.height());
        StdOut.println();
        // StdOut.println("The table gives the dual-gradient energies of each pixel.");
        // StdOut.println("The asterisks denote a minimum energy vertical or horizontal seam.");
        // StdOut.println();
        //
        // SeamCarver carver = new SeamCarver(picture);
        //
        // StdOut.printf("Vertical seam: { ");
        // int[] verticalSeam = carver.findVerticalSeam();
        // for (int x : verticalSeam)
        //     StdOut.print(x + " ");
        // StdOut.println("}");
        //
        // StdOut.printf("Horizontal seam: { ");
        // int[] horizontalSeam = carver.findHorizontalSeam();
        // for (int y : horizontalSeam)
        //     StdOut.print(y + " ");
        // StdOut.println("}");

        // int[] invalidVerticalSeam = new int[] { 10, 10, 11, 11, 11, 12, 11, 10, 10, 10 };
        // carver.removeVerticalSeam(invalidVerticalSeam);

        // int[] invalidHorizontalSeam = new int[] { 7, 8, 9, 10, 9, 8, 8, 8, 7, 6, 7, 8 };
        // carver.removeHorizontalSeam(invalidHorizontalSeam);
        SeamCarver carver = new SeamCarver(picture);
        carver.picture();
        for (int col = 0; col < 1; col++) {
            for (int row = 0; row < 8; row++) {
                carver.energy(col, row);
                carver.energy(col, row);
                carver.energy(col, row);
            }
        }
        carver.picture();
        carver.picture();
        carver.removeHorizontalSeam(new int[] {
                0
        });
        for (int col = 0; col < 1; col++) {
            for (int row = 0; row < 7; row++) {
                carver.energy(col, row);
                carver.energy(col, row);
                carver.energy(col, row);
            }
        }
    }
}
