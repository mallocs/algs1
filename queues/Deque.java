/* *****************************************************************************
 *  Name: Marcus Ulrich
 *  Some ideas from:
 *  https://algs4.cs.princeton.edu/13stacks/LinkedStack.java.html
 **************************************************************************** */

import edu.princeton.cs.algs4.StdOut;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {
    private int n;          // size of the deque
    private Node first;     // top of deque
    private Node last;     // bottom of deque


    // helper linked list class
    private class Node {
        private Item item;
        private Node next;
        private Node previous;
    }


    // construct an empty deque
    public Deque() {
        first = null;
        last = null;
        n = 0;
    }

    // is the deque empty?
    public boolean isEmpty() {
        return first == null;
    }

    // return the number of items on the deque
    public int size() {
        return n;
    }

    // add the item to the front
    public void addFirst(Item item) {
        if (item == null) {
            throw new IllegalArgumentException("Need an item to add");
        }
        Node oldfirst = first;
        first = new Node();
        first.item = item;
        if (oldfirst == null) {
            last = first;
        }
        else {
            first.next = oldfirst;
            oldfirst.previous = first;
        }
        n++;
    }

    // add the item to the back
    public void addLast(Item item) {
        if (item == null) {
            throw new IllegalArgumentException("Need an item to add");
        }
        Node oldlast = last;
        last = new Node();
        last.item = item;
        if (oldlast == null) {
            first = last;
        }
        else {
            last.previous = oldlast;
            oldlast.next = last;
        }
        n++;
    }

    // remove and return the item from the front
    public Item removeFirst() {
        if (first == null) {
            throw new NoSuchElementException();
        }
        Item item = first.item;
        first = first.next;
        if (first != null) {
            first.previous = null;
        }
        else {
            last = null;
        }
        n--;
        return item;
    }

    // remove and return the item from the back
    public Item removeLast() {
        if (last == null) {
            throw new NoSuchElementException();
        }
        Item item = last.item;
        last = last.previous;
        if (last != null) {
            last.next = null;
        }
        else {
            first = null;
        }
        n--;
        return item;
    }

    // return an iterator over items in order from front to back
    public Iterator<Item> iterator() {
        return new DequeIterator();
    }

    private class DequeIterator implements Iterator<Item> {
        private Node current = first;

        public boolean hasNext() {
            return current != null;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public Item next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            Item item = current.item;
            current = current.next;
            return item;
        }
    }

    // unit testing (required)
    public static void main(String[] args) {
        Deque<String> d1 = new Deque<String>();
        StdOut.println("Empty deque should be true: .isEmpty returns " + d1.isEmpty());
        d1.addFirst("a");
        d1.addFirst("b");
        d1.addFirst("c");
        StdOut.println("Size should be 3: .size returns " + d1.size());
        StdOut.print(".addFirst Iterator should return 3 items \"c b a\"\n");
        for (String str : d1) {
            StdOut.print(str + " ");
        }
        StdOut.println();
        d1.removeLast();
        StdOut.print(".removeLast: Iterator should return 2 items \"c b\"\n");
        for (String str : d1) {
            StdOut.print(str + " ");
        }
        StdOut.println();
        d1.removeFirst();
        StdOut.print(".removeFirst: Iterator should return 1 item \"b\"\n");
        for (String str : d1) {
            StdOut.print(str + " ");
        }
        StdOut.println();
        d1.addLast("z");
        d1.addFirst("a");
        StdOut.print(".addLast: Iterator should return 3 items \"a b z\"\n");
        for (String str : d1) {
            StdOut.print(str + " ");
        }
        StdOut.println();
        StdOut.println(
                "Two iterators should work at the same time. Should print \"\na-a a-b a-z\nb-a b-b b-z\nz-a z-b z-z \"\n");
        for (String a : d1) {
            for (String b : d1)
                StdOut.print(a + "-" + b + " ");
            StdOut.println();
        }
        StdOut.println("Check adding 1 and then removing one for new deque");
        Deque<Integer> d2 = new Deque<Integer>();

        d2.addFirst(1);
        d2.removeLast();
        d2.addFirst(3);
        d2.removeLast();

        StdOut.println("Check adding 2 and then removing one for new deque");
        Deque<Integer> d3 = new Deque<Integer>();
        d3.addFirst(1);
        d3.addFirst(2);
        d3.removeFirst();
        d3.removeFirst();
        d3.addLast(5);
    }

}
