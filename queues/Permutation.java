/* *****************************************************************************
 *  Name: Marcus Ulrich
 **************************************************************************** */

import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

public class Permutation {
    public static void main(String[] args) {
        RandomizedQueue<String> r = new RandomizedQueue<String>();
        int outputCount = Integer.parseInt(args[0]);
        while (!StdIn.isEmpty()) {
            String str = StdIn.readString();
            r.enqueue(str);
        }
        for (int index = 0; index < outputCount; index++) {
            String str = r.dequeue();
            StdOut.println(str);
        }
    }
}

