/* *****************************************************************************
 *  Name: Marcus Ulrich
 *  Some ideas from:
 *  https://algs4.cs.princeton.edu/13stacks/ResizingArrayStack.java.html
 **************************************************************************** */

import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class RandomizedQueue<Item> implements Iterable<Item> {
    // initial capacity of underlying resizing array
    private static final int INIT_CAPACITY = 8;

    private int n;          // size of the queue
    private Item[] items;     // queue

    // construct an empty randomized queue
    public RandomizedQueue() {
        items = (Item[]) new Object[INIT_CAPACITY];
        n = 0;
    }

    // is the randomized queue empty?
    public boolean isEmpty() {
        return size() == 0;
    }

    // return the number of items on the randomized queue
    public int size() {
        return n;
    }

    private Item[] copy(int capacity) {
        Item[] copy = (Item[]) new Object[capacity];
        for (int i = 0; i < size(); i++) {
            copy[i] = items[i];
        }
        return copy;
    }

    private void resize(int capacity) {
        // StdOut.println("copy to: " + capacity);
        items = copy(capacity);
    }

    // add the item
    public void enqueue(Item item) {
        if (item == null) {
            throw new IllegalArgumentException();
        }
        if (size() + 1 >= items.length) {
            resize(items.length * 2);
        }
        items[n] = item;
        n++;
    }

    // remove and return a random item
    public Item dequeue() {
        if (size() == 0) {
            throw new NoSuchElementException();
        }
        int removeIndex = StdRandom.uniform(size());

        Item removeItem = items[removeIndex];
        items[removeIndex] = items[n - 1];
        items[n - 1] = null;
        n--;
        if (n < (items.length / 4)) {
            resize(items.length / 4);
        }
        return removeItem;
    }

    // return a random item (but do not remove it)
    public Item sample() {
        if (size() == 0) {
            throw new NoSuchElementException();
        }
        return items[StdRandom.uniform(size())];
    }

    // return an independent iterator over items in random order
    public Iterator<Item> iterator() {
        return new RandomizedQueueIterator();
    }

    private class RandomizedQueueIterator implements Iterator<Item> {
        private final Item[] iteratorItems;
        private int count = 0;

        public RandomizedQueueIterator() {
            iteratorItems = (Item[]) new Object[size()];
            for (int i = 0; i < size(); i++) {
                iteratorItems[i] = items[i];
            }
            StdRandom.shuffle(iteratorItems);
        }

        public boolean hasNext() {
            return count < size();
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public Item next() {
            if (!hasNext()) throw new NoSuchElementException();
            Item next = iteratorItems[count];
            count++;
            return next;
        }
    }

    // unit testing (required)
    public static void main(String[] args) {
        RandomizedQueue<String> r = new RandomizedQueue<String>();
        StdOut
                .println("Empty randomized queue should be true: .isEmpty returns " + r.isEmpty());
        r.enqueue("a");
        r.enqueue("b");
        r.enqueue("c");
        StdOut.println("Size should be 3: .size returns " + r.size());
        StdOut
                .println("Non-empty randomized queue should be false: .isEmpty returns " + r
                        .isEmpty());
        StdOut.print("Iterator should return \"c b a\" item in random order \n");
        for (String str : r) {
            StdOut.print(str + " ");
        }
        StdOut.println();
        StdOut.println(".dequeue should return and remove a random item: " + r.dequeue());
        StdOut.println("so size should now be 2: " + r.size());
        r.enqueue("d");
        r.enqueue("e");
        r.enqueue("f");
        StdOut.println("Size should now be 5: " + r.size());
        StdOut.println(".sample should return a random item: " + r.sample());
        StdOut.println(".sample should not remove item so size should still be 5: " + r.size());
        for (int i = 0; i < 100; i++) {
            r.enqueue("" + i);
        }
        StdOut.println("should have 105 items: " + r.size());
        StdOut.println("should dequeue 100 items: ");
        for (int i = 0; i < 100; i++) {
            StdOut.print(r.dequeue() + " ");
        }
    }

}
