/* *****************************************************************************
 *  Name: Marcus Ulrich
 **************************************************************************** */

import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.SET;
import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdOut;

public class PointSET {
    private final SET<Point2D> set;

    // construct an empty set of points
    public PointSET() {
        set = new SET<Point2D>();
    }

    // is the set empty?
    public boolean isEmpty() {
        return set.isEmpty();
    }

    // number of points in the set
    public int size() {
        return set.size();
    }

    // add the point to the set (if it is not already in the set)
    public void insert(Point2D p) {
        if (p == null) {
            throw new IllegalArgumentException("No point specified for insert");
        }
        set.add(p);
    }

    // does the set contain point p?
    public boolean contains(Point2D p) {
        if (p == null) {
            throw new IllegalArgumentException("No point specified for contains");
        }
        return set.contains(p);
    }

    // draw all points to standard draw
    public void draw() {
        for (Point2D p : set) {
            p.draw();
        }
    }

    // all points that are inside the rectangle (or on the boundary)
    public Iterable<Point2D> range(RectHV rect) {
        if (rect == null) {
            throw new IllegalArgumentException("No rectangle specified for range");
        }
        Stack<Point2D> points = new Stack<Point2D>();
        for (Point2D p : set) {
            if (rect.contains(p)) {
                points.push(p);
            }
        }
        return points;
    }

    // a nearest neighbor in the set to point p; null if the set is empty
    public Point2D nearest(Point2D p) {
        if (p == null) {
            throw new IllegalArgumentException("No point specified for nearest");
        }
        if (isEmpty()) {
            return null;
        }

        Point2D nearest = null;
        double minDistance = -1;
        for (Point2D testPoint : set) {
            if (p.distanceSquaredTo(testPoint) < minDistance || minDistance == -1) {
                nearest = testPoint;
                minDistance = p.distanceSquaredTo(testPoint);
            }
        }
        return nearest;
    }

    // unit testing of the methods (optional)
    public static void main(String[] args) {
        PointSET set = new PointSET();
        set.insert(new Point2D(0.46484375, 0.1455078125));
        set.insert(new Point2D(0.8349609375, 0.482421875));
        StdOut.println(set.range(new RectHV(0.1181640625, 0.298828125, 0.56640625, 0.455078125))
                               + "==>  empty");
        StdOut.println(
                set.contains(new Point2D(0.3515625, 0.1552734375)) + " ==>  false");
        StdOut.println(set.nearest(
                new Point2D(0.23046875, 0.3466796875)) + " ==>  (0.46484375, 0.1455078125)");
        StdOut.println(set.size() + " ==>  2");
        set.insert(new Point2D(0.21875, 0.994140625));
        StdOut.println(
                set.nearest(new Point2D(0.2158203125, 0.546875)) + " ==>  (0.21875, 0.994140625)");
        set.insert(new Point2D(0.8974609375, 0.619140625));
        set.insert(new Point2D(0.08984375, 0.4853515625));
        StdOut.println(set.range(new RectHV(0.50390625, 0.703125, 0.927734375, 0.8349609375))
                               + "==>  empty");
        StdOut.println(set.range(new RectHV(0.3330078125, 0.4130859375, 0.53125, 0.9892578125))
                               + "==>  empty");
        StdOut.println(set.range(new RectHV(0.33984375, 0.232421875, 0.3740234375, 0.6591796875))
                               + "==>  empty");
        StdOut.println(set.isEmpty() + " ==>  false");
        set.insert(new Point2D(0.927734375, 0.982421875));
        StdOut.println(set.isEmpty() + " ==>  false");
        StdOut.println(set.size() + " ==>  6");
        set.insert(new Point2D(0.47265625, 0.998046875));
        StdOut.println(set.nearest(
                new Point2D(0.189453125, 0.060546875)) + " ==>  (0.46484375, 0.1455078125)");

    }


}
