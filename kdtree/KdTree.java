/* *****************************************************************************
 *  Name: Marcus Ulrich
 **************************************************************************** */

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

public class KdTree {
    private KdTree.Node root;
    private int rootSize = 0;

    // construct an empty of KdTree
    public KdTree() {
    }

    // is the set empty?
    public boolean isEmpty() {
        return size() == 0;
    }

    // number of points in the set
    public int size() {
        return rootSize;
    }

    // add the point to the set (if it is not already in the set)
    public void insert(Point2D p) {
        if (p == null) {
            throw new IllegalArgumentException("No point specified for insert");
        }
        root = insert(root, p, true, 0, 0, 1, 1);
    }

    private boolean goLeft(KdTree.Node n, Point2D p, boolean useXCoordinate) {
        return useXCoordinate ? p.x() < n.p.x() : p.y() < n.p.y();
    }

    private KdTree.Node insert(KdTree.Node n, Point2D p, boolean useXCoordinate, double xmin,
                               double ymin, double xmax, double ymax) {
        if (n == null) {
            rootSize += 1;
            return new KdTree.Node(p, new RectHV(xmin, ymin, xmax, ymax));
        }
        if (p.equals(n.p)) {
            return n;
        }
        if (goLeft(n, p, useXCoordinate)) {
            if (useXCoordinate) {
                n.lb = insert(n.lb, p, !useXCoordinate, xmin, ymin, n.p.x(), ymax);
            }
            else {
                n.lb = insert(n.lb, p, !useXCoordinate, xmin, ymin, xmax, n.p.y());
            }
        }
        else {
            if (useXCoordinate) {
                n.rt = insert(n.rt, p, !useXCoordinate, n.p.x(), ymin, xmax, ymax);
            }
            else {
                n.rt = insert(n.rt, p, !useXCoordinate, xmin, n.p.y(), xmax, ymax);
            }
        }
        return n;
    }

    // does the set contain point p?
    public boolean contains(Point2D p) {
        if (p == null) {
            throw new IllegalArgumentException("No point specified for contains");
        }
        return get(p);
    }

    private boolean get(Point2D p) {
        return get(root, p, true);
    }

    private boolean get(KdTree.Node n, Point2D p, boolean useXCoordinate) {
        if (n == null) {
            return false;
        }
        if (p == null) {
            throw new IllegalArgumentException("No point specified for get");
        }
        if (p.equals(n.p)) {
            return true;
        }
        if (goLeft(n, p, useXCoordinate)) {
            return get(n.lb, p, !useXCoordinate);
        }
        else {
            return get(n.rt, p, !useXCoordinate);
        }
    }

    // draw all points to standard draw
    public void draw() {
        StdDraw.enableDoubleBuffering();
        draw(root, true);
        StdDraw.show();

    }

    private void draw(KdTree.Node n, boolean useXCoordinate) {
        if (n == null) {
            return;
        }
        if (useXCoordinate) {
            StdDraw.setPenColor(StdDraw.RED);
            StdDraw.setPenRadius(0.01);
            StdDraw.line(n.p.x(), n.rect.ymin(), n.p.x(), n.rect.ymax());
        }
        else {
            StdDraw.setPenColor(StdDraw.BLUE);
            StdDraw.setPenRadius(0.01);
            StdDraw.line(n.rect.xmin(), n.p.y(), n.rect.xmax(), n.p.y());
        }
        draw(n.lb, !useXCoordinate);
        draw(n.rt, !useXCoordinate);
        StdDraw.setPenColor(StdDraw.BLACK);
        n.p.draw();
    }


    // all points that are inside the rectangle (or on the boundary)
    public Iterable<Point2D> range(RectHV rect) {
        if (rect == null) {
            throw new IllegalArgumentException("No rectangle specified for range");
        }
        Queue<Point2D> queue = new Queue<Point2D>();
        if (isEmpty()) {
            return queue;
        }
        return range(rect, root, queue, true);
    }

    private Iterable<Point2D> range(RectHV rect, KdTree.Node n, Queue<Point2D> queue,
                                    boolean useXCoordinate) {
        if (rect.contains(n.p)) {
            queue.enqueue(n.p);
        }
        if (n.lb != null && rect.intersects(n.lb.rect)) {
            range(rect, n.lb, queue, !useXCoordinate);
        }
        if (n.rt != null && rect.intersects(n.rt.rect)) {
            range(rect, n.rt, queue, !useXCoordinate);
        }
        return queue;
    }

    // a nearest neighbor in the set to point p; null if the set is empty
    public Point2D nearest(Point2D queryPoint) {
        if (queryPoint == null) {
            throw new IllegalArgumentException("No point specified for nearest");
        }
        if (isEmpty()) {
            return null;
        }
        return nearest(root, queryPoint, root.p, root.p.distanceSquaredTo(queryPoint), true);
    }

    private Point2D nearest(KdTree.Node n, Point2D queryPoint, Point2D nearest,
                            double nearestDistance,
                            boolean useXCoordinate) {
        if (n == null) {
            return nearest;
        }
        if (nearestDistance < n.rect.distanceSquaredTo(queryPoint)) {
            return nearest;
        }
        double distance = n.p.distanceSquaredTo(queryPoint);
        if (distance < nearestDistance) {
            nearest = n.p;
            nearestDistance = queryPoint.distanceSquaredTo(nearest);
        }
        if (goLeft(n, queryPoint, useXCoordinate)) {
            nearest = nearest(n.lb, queryPoint, nearest, nearestDistance, !useXCoordinate);
            nearestDistance = queryPoint.distanceSquaredTo(nearest);
            return nearest(n.rt, queryPoint, nearest, nearestDistance, !useXCoordinate);
        }
        else {
            nearest = nearest(n.rt, queryPoint, nearest, nearestDistance, !useXCoordinate);
            nearestDistance = queryPoint.distanceSquaredTo(nearest);
            return nearest(n.lb, queryPoint, nearest, nearestDistance, !useXCoordinate);
        }
    }

    private static class Node {
        private final Point2D p;      // the point
        private final RectHV rect;    // the axis-aligned rectangle corresponding to this node
        private Node lb;        // the left/bottom subtree
        private Node rt;        // the right/top subtree

        public Node(Point2D p, RectHV rect) {
            this.p = p;
            this.rect = rect;
        }
    }

    // unit testing of the methods (optional)
    public static void main(String[] args) {
        KdTree kdtree = new KdTree();
        Point2D p1 = new Point2D(0.7, 0.2);
        Point2D p2 = new Point2D(0.5, 0.4);
        Point2D p3 = new Point2D(0.2, 0.3);
        Point2D p3Duplicate = new Point2D(0.2, 0.3);
        Point2D p4 = new Point2D(0.4, 0.7);
        Point2D p5 = new Point2D(0.9, 0.6);
        kdtree.insert(p1);
        kdtree.insert(p2);
        StdOut.println("KdTree should contain point p1 after insert. Should be true: " + kdtree
                .contains(p1));
        StdOut.println("KdTree should contain point p2 after insert. Should be true: " + kdtree
                .contains(p2));
        StdOut.println(
                "KdTree should not contain point p3. Should be false: " + kdtree.contains(p3));
        StdOut.println(
                "KdTree size should be 2: " + kdtree.size());
        kdtree.insert(p3);
        StdOut.println(
                "KdTree size should now be 3: " + kdtree.size());
        kdtree.insert(p3Duplicate);
        StdOut.println(
                "KdTree size should still be 3 after inserting a duplicate: " + kdtree.size());
        kdtree.insert(p4);
        kdtree.insert(p5);
        // kdtree.draw();

        KdTree nearestTest = new KdTree();
        Point2D pointa = new Point2D(0.75, 1.0);
        Point2D pointb = new Point2D(0.125, 0.0);
        Point2D pointc = new Point2D(0.0, 0.125);
        Point2D pointd = new Point2D(1.0, 0.375);
        Point2D pointe = new Point2D(0.5, 0.75);
        nearestTest.insert(pointa);
        nearestTest.insert(pointb);
        nearestTest.insert(pointc);
        nearestTest.insert(pointd);
        nearestTest.insert(pointe);
        Point2D nearestTestPoint = new Point2D(0.625, 0.25);
        StdOut.println(
                ".nearest(new Point2D(0.625, 0.25) should be (1.0, 0.375): " + nearestTest
                        .nearest(nearestTestPoint));
        nearestTest.draw();

        if (args.length == 1) {
            String filename = args[0];
            StdOut.println(filename + " <-filename");
            In in = new In(filename);
            KdTree kdtreeIn = new KdTree();
            while (!in.isEmpty()) {
                double x = in.readDouble();
                double y = in.readDouble();
                Point2D p = new Point2D(x, y);
                kdtreeIn.insert(p);
            }
            StdOut.println("Should be true: " + kdtreeIn.contains(new Point2D(0.768734, 0.597130)));
            StdOut.println("Should be 0.768734, 0.597130: " + kdtreeIn
                    .nearest(new Point2D(0.768733, 0.597129)));
        }
    }
}
