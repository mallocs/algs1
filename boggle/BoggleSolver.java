/* *****************************************************************************
 *  Name: Marcus Ulrich
 **************************************************************************** */

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

public class BoggleSolver {
    // private Trie prunedDict = new Trie();
    private final Trie dict = new Trie();
    private boolean[][] marked;
    // keep track of whether we have a tile for that character


    // Initializes the data structure using the given array of strings as the dictionary.
    // (You can assume each word in the dictionary contains only the uppercase letters A through Z.)
    public BoggleSolver(String[] dictionary) {
        for (int i = 0; i < dictionary.length; i++) {
            dict.add(dictionary[i]);
        }
    }


    // Returns the set of all valid words in the given Boggle board, as an Iterable.
    public Iterable<String> getAllValidWords(BoggleBoard board) {
        Trie found = new Trie();
        marked = new boolean[board.rows()][board.cols()];

        for (int i = 0; i < board.rows(); i++) {
            for (int j = 0; j < board.cols(); j++) {
                String str;
                if (board.getLetter(i, j) != 81) {
                    str = board.getLetter(i, j) + "";
                }
                else {
                    str = "QU";
                }
                Trie.Node x = dict.get(dict.getRoot(), str, 0);
                getAllValidWordsHelper(board,
                                       str, i, j,
                                       found, x);
            }
        }
        return found;
    }


    // mutates
    private void getAllValidWordsHelper(BoggleBoard board,
                                        String str, int row,
                                        int col,
                                        Trie found, Trie.Node x) {
        marked[row][col] = true;

        if (str.length() > 2 && dict.contains(str, x)) {
            found.add(str);
        }
        if (!dict.nodeHasKeys(x)) {
            marked[row][col] = false;
            return;
        }
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                int nextRow = row + i;
                int nextCol = col + j;
                if (nextRow >= 0 && nextRow < board.rows() && nextCol >= 0
                        && nextCol < board.cols() && !marked[nextRow][nextCol]) {
                    String nextStr;
                    if (board.getLetter(nextRow, nextCol) != 81) {
                        nextStr = str + board.getLetter(nextRow, nextCol);
                    }
                    else {
                        nextStr = str + "QU";
                    }

                    getAllValidWordsHelper(board,
                                           nextStr, nextRow,
                                           nextCol, found,
                                           dict.get(x, nextStr, str.length()));
                }
            }
        }
        marked[row][col] = false;
        return;
    }

    // Returns the score of the given word if it is in the dictionary, zero otherwise.
    // (You can assume the word contains only the uppercase letters A through Z.)
    public int scoreOf(String word) {
        if (!dict.contains(word)) {
            return 0;
        }
        switch (word.length()) {
            case 1:
            case 2:
                return 0;
            case 3:
            case 4:
                return 1;
            case 5:
                return 2;
            case 6:
                return 3;
            case 7:
                return 5;
            default:
                return 11;
        }
    }

    public static void main(String[] args) {
        In in = new In(args[0]);
        String[] dictionary = in.readAllStrings();
        BoggleSolver solver = new BoggleSolver(dictionary);
        BoggleBoard board = new BoggleBoard(args[1]);
        int score = 0;
        int wordCount = 0;
        for (String word : solver.getAllValidWords(board)) {
            wordCount++;
            StdOut.println(word);
            score += solver.scoreOf(word);
        }
        StdOut.println("Score = " + score);
        StdOut.println("Wordcount is " + wordCount);
        solver.getAllValidWords(board);
        for (int i = 0; i < 10000; i++) {
            BoggleBoard board2 = new BoggleBoard(args[2]);
            wordCount = 0;
            score = 0;
            for (String word : solver.getAllValidWords(board2)) {
                wordCount++;
                // StdOut.println(word);
                score += solver.scoreOf(word);
            }
            // StdOut.println("Iteration: " + i);
            // StdOut.println("Score = " + score);
            // StdOut.println("Wordcount is " + wordCount);
        }
    }
}
