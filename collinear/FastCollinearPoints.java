/* *****************************************************************************
 *  Name: Marcus Ulrich
 **************************************************************************** */

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.ResizingArrayStack;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

import java.util.Arrays;

public class FastCollinearPoints {
    private final ResizingArrayStack<LineSegment> ss = new ResizingArrayStack<LineSegment>();

    // finds all line segments containing 4 or more points
    public FastCollinearPoints(Point[] points) {
        if (points == null || containsNullPoints(points)) {
            throw new IllegalArgumentException("Contains null points");
        }
        Point[] allPoints = copyPoints(points);
        Arrays.sort(allPoints);
        if (containsDuplicatedPoints(allPoints)) {
            throw new IllegalArgumentException(
                    "Contains duplicate points"); //  + printPoints(points));
        }

        for (Point point : points) {
            setCollinearSegmentsFromPoint(point, copyPoints(points));
        }
    }

    private boolean containsNullPoints(Point[] points) {
        for (int i = 0; i < points.length; i++) {
            if (points[i] == null) {
                return true;
            }
        }
        return false;
    }

    private boolean containsDuplicatedPoints(Point[] points) {
        // must be sorted
        for (int i = 1; i < points.length; i++) {
            if (points[i - 1].compareTo(points[i]) == 0) {
                return true;
            }
        }
        return false;
    }

    // private String printPoints(Point[] points) {
    //     String str = "";
    //     if (points != null) {
    //         for (Point p : points) {
    //             str += p;
    //         }
    //     }
    //     StdOut.println(str);
    //     return str;
    // }

    private void setCollinearSegmentsFromPoint(Point origin, Point[] points) {
        if (points.length < 3) {
            return;
        }
        Arrays.sort(points, origin.slopeOrder());
        for (int i = 1, startIndex = -1; i < points.length - 1; i++) {
            Point p1 = points[i];
            double slope1 = origin.slopeTo(p1);
            Point p2 = points[i + 1];
            double slope2 = origin.slopeTo(p2);
            if (slope1 == slope2 && startIndex == -1) {
                startIndex = i;
            }
            else if (slope1 != slope2 && startIndex != -1) {
                processRange(points, origin, startIndex, i + 1);
                startIndex = -1;
            }
            else if (i == points.length - 2 && startIndex != -1) {
                processRange(points, origin, startIndex, i + 2);
            }
        }
    }

    private void processRange(Point[] points, Point origin, int startIndex, int endIndex) {
        if (endIndex - startIndex >= 3) {
            Point[] endpoints = getLineEndpoints(points, origin, startIndex, endIndex);
            if (endpoints[0].compareTo(origin) == 0) {
                ss.push(new LineSegment(endpoints[0], endpoints[1]));
            }
        }
    }

    private Point[] getLineEndpoints(Point[] points, Point origin, int startIndex,
                                     int endIndexExclusive) {
        Point least = origin;
        Point greatest = origin;
        for (int i = startIndex; i < endIndexExclusive; i++) {
            Point testPoint = points[i];
            if (testPoint.compareTo(least) < 0) {
                least = testPoint;
            }
            if (testPoint.compareTo(greatest) > 0) {
                greatest = testPoint;
            }
        }

        return new Point[] { least, greatest };
    }

    private Point[] copyPoints(Point[] points) {
        Point[] copy = new Point[points.length];
        for (int i = 0; i < points.length; i++) {
            copy[i] = points[i];
        }
        return copy;
    }

    // the number of line segments
    public int numberOfSegments() {
        return ss.size();
    }

    // the line segments
    public LineSegment[] segments() {
        LineSegment[] segs = new LineSegment[numberOfSegments()];
        int i = 0;
        for (LineSegment seg : ss) {
            segs[i] = seg;
            i++;
        }
        return segs;
    }

    public static void main(String[] args) {
        // read the n points from a file
        In in = new In(args[0]);
        int n = in.readInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }

        // draw the points
        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
        }
        StdDraw.show();

        // print and draw the line segments
        FastCollinearPoints collinear = new FastCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }
        StdDraw.show();
    }
}
