/* *****************************************************************************
 *  Name: Marcus Ulrich
 **************************************************************************** */

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.ResizingArrayStack;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

import java.util.Arrays;

public class BruteCollinearPoints {
    private final ResizingArrayStack<LineSegment> ss = new ResizingArrayStack<LineSegment>();
    // private final Point[] allPoints;

    public BruteCollinearPoints(Point[] points) {    // finds all line segments containing 4 points
        if (points == null || containsNullPoints(points)) {
            throw new IllegalArgumentException("Bad points!");
        }
        Point[] allPoints = copyPoints(points);
        Arrays.sort(allPoints);
        if (containsDuplicatedPoints(allPoints)) {
            throw new IllegalArgumentException("Invalid points! "); //  + printPoints(points));
        }

        int lineLength = 4;
        setSegments(allPoints, lineLength, 0, new Point[lineLength]);
    }

    private Point[] copyPoints(Point[] points) {
        Point[] copy = new Point[points.length];
        for (int i = 0; i < points.length; i++) {
            copy[i] = points[i];
        }
        return copy;
    }

    private boolean containsNullPoints(Point[] points) {
        for (int i = 0; i < points.length; i++) {
            if (points[i] == null) {
                return true;
            }
        }
        return false;
    }

    private boolean containsDuplicatedPoints(Point[] points) {
        // must be sorted
        for (int i = 1; i < points.length; i++) {
            if (points[i - 1].compareTo(points[i]) == 0) {
                return true;
            }
        }
        return false;
    }

    private boolean areAscending(Point[] points) {
        for (int i = 1; i < points.length; i++) {
            if (points[i - 1].compareTo(points[i]) > 0) {
                return false;
            }
        }
        return true;
    }

    private void setSegments(Point[] points, int len, int startPosition, Point[] result) {
        if (len == 0) {
            Arrays.sort(result);
            if (result[0].slopeTo(result[1]) == result[1].slopeTo(result[2])
                    && result[1].slopeTo(result[2]) == result[2].slopeTo(result[3]) && areAscending(
                    result)) {
                // StdOut.println("pushing points: " + printPoints(result));

                LineSegment line = new LineSegment(result[0], result[result.length - 1]);
                ss.push(line);
            }
            return;
        }
        for (int i = startPosition; i <= points.length - len; i++) {
            result[result.length - len] = points[i];
            setSegments(points, len - 1, i + 1, result);
        }
    }

    // private String printPoints(Point[] points) {
    //     String str = "";
    //     if (points != null) {
    //         for (Point p : points) {
    //             str += p;
    //         }
    //     }
    //     return str;
    // }

    // the number of line segments
    public int numberOfSegments() {
        return ss.size();
    }

    // the line segments
    public LineSegment[] segments() {
        LineSegment[] segs = new LineSegment[numberOfSegments()];
        int i = 0;
        for (LineSegment seg : ss) {
            segs[i] = seg;
            i++;
        }
        return segs;
    }

    public static void main(String[] args) {
        // read the n points from a file
        In in = new In(args[0]);
        int n = in.readInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }

        // draw the points
        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
        }
        StdDraw.show();

        // print and draw the line segments
        BruteCollinearPoints collinear = new BruteCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }
        StdDraw.show();
    }
}
