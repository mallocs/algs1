/* *****************************************************************************
 *  Name: Marcus Ulrich
 **************************************************************************** */

import edu.princeton.cs.algs4.Bag;
import edu.princeton.cs.algs4.FlowEdge;
import edu.princeton.cs.algs4.FlowNetwork;
import edu.princeton.cs.algs4.FordFulkerson;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.ST;
import edu.princeton.cs.algs4.StdOut;

public class BaseballElimination {

    private String[] teams;
    private ST<String, Integer> teamsST;
    private int[] wins;
    private int[] loses;
    private int[] remainingTotal;
    private int[][] remainingVersus;

    // create a baseball division from given filename in format specified below
    public BaseballElimination(String filename) {
        readInput(filename);
    }

    private void readInput(String filename) {
        In inputStream = new In(filename);
        int teamCount = inputStream.readInt();
        inputStream.readLine();

        teamsST = new ST<String, Integer>();
        teams = new String[teamCount];
        wins = new int[teamCount];
        loses = new int[teamCount];
        remainingTotal = new int[teamCount];
        remainingVersus = new int[teamCount][teamCount];

        int teamIndex = 0;
        while (inputStream.hasNextLine()) {
            String line = inputStream.readLine().trim();
            String[] fields = line.split("\\s+");
            teams[teamIndex] = fields[0];
            teamsST.put(fields[0], teamIndex);
            wins[teamIndex] = Integer.parseInt(fields[1]);
            loses[teamIndex] = Integer.parseInt(fields[2]);
            remainingTotal[teamIndex] = Integer.parseInt(fields[3]);
            for (int i = 0; i < teamCount; i++) {
                remainingVersus[teamIndex][i] = Integer.parseInt(fields[(4 + i)]);
            }
            teamIndex++;
        }

    }

    private void validateTeam(String team) {
        if (!teamsST.contains(team)) {
            throw new IllegalArgumentException("Invalid team");
        }
    }

    // number of teams
    public int numberOfTeams() {
        return teams.length;
    }

    // all teams
    public Iterable<String> teams() {
        return teamsST;
    }

    // number of wins for given team
    public int wins(String team) {
        validateTeam(team);
        return wins[teamsST.get(team)];
    }

    // number of losses for given team
    public int losses(String team) {
        validateTeam(team);
        return loses[teamsST.get(team)];
    }

    // number of remaining games for given team
    public int remaining(String team) {
        validateTeam(team);
        return remainingTotal[teamsST.get(team)];
    }

    // number of remaining games between team1 and team2
    public int against(String team1, String team2) {
        validateTeam(team1);
        validateTeam(team2);
        return remainingVersus[teamsST.get(team1)][teamsST.get(team2)];
    }

    // subset R of teams that eliminates given team; null if not eliminated
    public Iterable<String> certificateOfElimination(String team) {
        validateTeam(team);

        Bag<String> eliminators = new Bag<String>();
        int gamesRemainingCombinations = (teams.length - 1) * (teams.length - 2)
                / 2;
        // s + t + team vertices + games remaining vertices
        int sizeOfFlowNetwork = 2 + (teams.length - 1) + gamesRemainingCombinations;
        FlowNetwork fn = new FlowNetwork(sizeOfFlowNetwork);
        // s => sizeOfFlowNetwork - 1
        int sVertex = sizeOfFlowNetwork - 2;
        // t => sizeOfFlowNetwork
        int tVertex = sizeOfFlowNetwork - 1;
        // teams => 0 to teams.length
        // gamesRemaining => teams.length to sizeOfFlowNetwork - 1

        int eliminatedTeamMax = wins(team) + remaining(team);
        int eliminatedTeamIndex = teamsST.get(team);

        // add edge for teamsVertex => t
        for (int i = 0, vertexIndex = 0; i < teams.length; i++) {
            if (i == eliminatedTeamIndex) {
                continue;
            }
            int worstCase = eliminatedTeamMax - wins(teams[i]);
            // trivial elimination
            if (worstCase < 0) {
                eliminators.add(teams[i]);
                return eliminators;
            }
            fn.addEdge(new FlowEdge(vertexIndex, tVertex, worstCase));
            vertexIndex++;
        }

        // add vertices for s => gamesRemainingVertex
        // and gamesRemainingVertex => teamVertex
        for (int i = 0, vertexIndex = 0; i < teams.length; i++) {
            for (int j = 0; j < teams.length; j++) {
                if (i >= j || i == eliminatedTeamIndex || j == eliminatedTeamIndex) {
                    continue;
                }

                // offset + vertexIndex
                int versusIndex = (teams.length - 1) + vertexIndex;

                fn.addEdge(
                        new FlowEdge(sVertex, versusIndex, against(teams[i], teams[j])));
                // need to translate i and j to actual vertex index
                fn.addEdge(
                        new FlowEdge(versusIndex, i >= eliminatedTeamIndex ? i - 1 : i,
                                     Double.POSITIVE_INFINITY));
                fn.addEdge(
                        new FlowEdge(versusIndex, j >= eliminatedTeamIndex ? j - 1 : j,
                                     Double.POSITIVE_INFINITY));
                vertexIndex++;
            }
        }
        FordFulkerson maxflow = new FordFulkerson(fn, sVertex, tVertex);
        for (int v = 0, teamIndex = 0; v < teams.length - 1; v++, teamIndex++) {
            if (v == eliminatedTeamIndex) {
                teamIndex++;
            }
            if (maxflow.inCut(v)) {
                eliminators.add(teams[teamIndex]);
            }
        }
        return eliminators.size() == 0 ? null : eliminators;
    }

    // is given team eliminated?
    public boolean isEliminated(String team) {
        return certificateOfElimination(team) != null;
    }


    public static void main(String[] args) {
        BaseballElimination division = new BaseballElimination(args[0]);
        if (args.length == 2) {
            StdOut.println();
            StdOut.println("Is eliminated: " + division.isEliminated(args[1]));
            for (String t : division.certificateOfElimination(args[1])) {
                StdOut.print(t + " ");
            }
            return;
        }
        for (String team : division.teams()) {
            if (division.isEliminated(team)) {
                StdOut.print(team + " is eliminated by the subset R = { ");
                for (String t : division.certificateOfElimination(team)) {
                    StdOut.print(t + " ");
                }
                StdOut.println("}");
            }
            else {
                StdOut.println(team + " is not eliminated");
            }
        }

    }
}
