/* *****************************************************************************
 *  Name: Marcus Ulrich
 **************************************************************************** */

import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;

import java.util.LinkedList;

public class MoveToFront {
    private static final int R = 256;
    private static final int LG_R = 8;

    // apply move-to-front encoding, reading from standard input and writing to standard output
    public static void encode() {
        // c = (char) (b & 0xff)
        LinkedList<Character> sequence = new LinkedList<Character>();
        for (int i = 0; i < 256; i++) {
            sequence.addLast((char) i);
        }
        while (!BinaryStdIn.isEmpty()) {
            char in = BinaryStdIn.readChar();

            int position = sequence.indexOf(in);
            sequence.remove(position);
            sequence.addFirst(in);
            BinaryStdOut.write(position, LG_R);
        }
        BinaryStdOut.close();
    }

    // apply move-to-front decoding, reading from standard input and writing to standard output
    public static void decode() {
        LinkedList<Character> sequence = new LinkedList<Character>();
        for (int i = 0; i < 256; i++) {
            sequence.addLast((char) i);
        }
        while (!BinaryStdIn.isEmpty()) {
            int in = BinaryStdIn.readInt(LG_R);
            // A  B  R  A  C  A  D  A  B  R  A  !
            // 41 42 52 02 44 01 45 01 04 04 02 26
            char character = (char) (sequence.get(in) & 0xff);
            sequence.remove(in);
            sequence.addFirst(character);
            BinaryStdOut.write(character);
        }
        BinaryStdOut.close();
    }

    // if args[0] is "-", apply move-to-front encoding
    // if args[0] is "+", apply move-to-front decoding
    public static void main(String[] args) {
        if (args[0].equals("-")) encode();
        else if (args[0].equals("+")) decode();
        else throw new IllegalArgumentException("Illegal command line argument");
    }

}
