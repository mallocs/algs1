/* *****************************************************************************
 *  Name: Marcus Ulrich
 **************************************************************************** */

import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;

public class BurrowsWheeler {
    private static final int LG_R = 8;
    private static final int R = 256;

    // apply Burrows-Wheeler transform,
    // reading from standard input and writing to standard output
    public static void transform() {
        String str = BinaryStdIn.readString();
        CircularSuffixArray cfa = new CircularSuffixArray(str);
        char[] coded = new char[str.length()];
        for (int i = 0; i < str.length(); i++) {
            int currentIndex = cfa.index(i);
            if (currentIndex == 0) {
                BinaryStdOut.write(i, 32);
            }
            // get mod with negatives mapping to positive to handle -1
            //  ((n % m) + m) % m;
            coded[i] = str
                    .charAt((((currentIndex - 1) % str.length()) + str.length()) % str.length());
        }

        BinaryStdOut.write(new String(coded));
        BinaryStdOut.close();
    }

    // apply Burrows-Wheeler inverse transform,
    // reading from standard input and writing to standard output
    public static void inverseTransform() {

        int first = BinaryStdIn.readInt();
        String str = BinaryStdIn.readString();
        char[] t = str.toCharArray();
        int[] next = new int[t.length];

        StringBuilder output = new StringBuilder("");
        int[] count = new int[R + 1];
        char[] f = new char[t.length];


        for (int i = 0; i < t.length; i++) {
            count[t[i] + 1]++;
        }
        for (int r = 0; r < R; r++) {
            count[r + 1] += count[r];
        }
        for (int i = 0; i < t.length; i++) {
            next[count[t[i]]] = i;
            f[count[t[i]]++] = t[i];
        }

        for (int i = 0, outputIndex = first; i < f.length; i++) {
            output.append(f[outputIndex]);
            outputIndex = next[outputIndex];
        }
        BinaryStdOut.write(output.toString());
        BinaryStdOut.close();

    }


    // if args[0] is "-", apply Burrows-Wheeler transform
    // if args[0] is "+", apply Burrows-Wheeler inverse transform
    public static void main(String[] args) {
        if (args[0].equals("-")) transform();
        else if (args[0].equals("+")) inverseTransform();
        else throw new IllegalArgumentException("Illegal command line argument");
    }

}
