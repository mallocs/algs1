/* *****************************************************************************
 *  Name: Marcus Ulrich
 **************************************************************************** */

import edu.princeton.cs.algs4.StdOut;

import java.util.Arrays;

public class CircularSuffixArray {
    private final String str;
    private final CircularSuffix[] suffixes;

    private class CircularSuffix implements Comparable<CircularSuffix> {
        private final int startIndex;

        public CircularSuffix(int i) {
            startIndex = i;
        }


        public int compareTo(CircularSuffix s) {
            for (int i = 0; i < str.length(); i++) {
                char me = str.charAt((startIndex + i) % str.length());
                char that = str.charAt((s.startIndex + i) % str.length());

                if (me != that) {
                    return Integer.compare(me, that);
                }
            }
            return 0;
        }
    }

    // circular suffix array of s
    public CircularSuffixArray(String s) {
        if (s == null) {
            throw new IllegalArgumentException("No string passed");
        }
        str = s;
        suffixes = new CircularSuffix[s.length()];
        for (int i = 0; i < s.length(); i++) {
            suffixes[i] = new CircularSuffix(i);
        }
        Arrays.sort(suffixes);

    }

    // length of s
    public int length() {
        return str.length();
    }

    // returns index of ith sorted suffix
    public int index(int i) {
        if (i >= str.length() || i < 0) {
            throw new IllegalArgumentException("Index not in range");
        }
        return suffixes[i].startIndex;
    }

    // unit testing (required)
    public static void main(String[] args) {
        CircularSuffixArray abra = new CircularSuffixArray("ABRACADABRA!");
        StdOut.println("Circular suffex array for: ABRACADABRA!");
        StdOut.print("Length should be 12: " + abra.length() + " ");
        StdOut.println(abra.length() == 12 ? "Pass" : "Fail");
        StdOut.print("index[3] should be 0: " + abra.index(3) + " ");
        StdOut.println(abra.index(3) == 0 ? "Pass" : "Fail");
        StdOut.print("index[0] should be 11: " + abra.index(0) + " ");
        StdOut.println(abra.index(0) == 11 ? "Pass" : "Fail");
        StdOut.print("index[1] should be 10: " + abra.index(1) + " ");
        StdOut.println(abra.index(1) == 10 ? "Pass" : "Fail");
        CircularSuffixArray aaaaaaa = new CircularSuffixArray("AAAAAAA");
        StdOut.print("index[0] should be 0: " + abra.index(0) + " ");
        StdOut.println(aaaaaaa.index(0) == 0 ? "Pass" : "Fail");
    }

}
