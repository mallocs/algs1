/******************************************************************************
 * Author: Marcus Ulrich
 ******************************************************************************/

import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;


public class PercolationStats {
    private static final double CI_95 = 1.96;

    private final int trials;
    private final int gridSize;
    private final double[] results;

    // perform independent trials on an n-by-n grid
    public PercolationStats(int n, int trials) {
        if (n <= 0 || trials <= 0) {
            throw new IllegalArgumentException("n and trails must be greater than zero");
        }
        results = new double[trials];
        this.trials = trials;
        this.gridSize = n;
        for (int i = 0; i < trials; i++) {
            results[i] = performExperiment();
        }
    }

    private double performExperiment() {
        Percolation perc = new Percolation(gridSize);
        int[] sites = new int[gridSize * gridSize];
        for (int i = 0; i < sites.length; i++) {
            sites[i] = i;
        }
        StdRandom.shuffle(sites);

        int i = 0;
        while (!perc.percolates()) {
            perc.open(Math.floorDiv(sites[i], gridSize) + 1, sites[i] % gridSize + 1);
            i++;
        }
        double numberOfSitesOpen = perc.numberOfOpenSites();
        return numberOfSitesOpen / (gridSize * gridSize);
    }

    // sample mean of percolation threshold
    public double mean() {
        return StdStats.mean(results); // / (gridSize * gridSize);
    }

    // sample standard deviation of percolation threshold
    public double stddev() {
        return StdStats.stddev(results); // / (gridSize * gridSize);
    }

    // low endpoint of 95% confidence interval
    public double confidenceLo() {
        double trialsAsDouble = trials;
        return mean() - (CI_95 * stddev() / Math.sqrt(trialsAsDouble));
    }

    // high endpoint of 95% confidence interval
    public double confidenceHi() {
        double trialsAsDouble = trials;
        return mean() + (CI_95 * stddev() / Math.sqrt(trialsAsDouble));
    }

    // test client (see below)
    public static void main(String[] args) {
        PercolationStats stats = new PercolationStats(Integer.parseInt(args[0]),
                                                      Integer.parseInt((args[1])));
        System.out.println("mean                    = " + stats.mean());
        System.out.println("stddev                  = " + stats.stddev());
        System.out.println("95% confidence interval = [" + stats.confidenceLo() + ", " + stats
                .confidenceHi() + "]");
    }

}
