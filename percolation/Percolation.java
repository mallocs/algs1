/******************************************************************************
 * Author: Marcus Ulrich
 ******************************************************************************/

import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {
    private final int gridSize;
    private final int virtualBottomIndex;
    private final int virtualTopIndex;
    private boolean[][] grid;
    private int totalOpen = 0;
    private final WeightedQuickUnionUF connected;
    private final WeightedQuickUnionUF connectedToTop;

    // creates n-by-n grid, with all sites initially blocked
    public Percolation(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException(
                    "Grid size must be greater than 0");
        }
        gridSize = n;
        int gridIndexBounds = gridSize + 1;
        grid = new boolean[gridIndexBounds][gridIndexBounds];
        virtualTopIndex = 0;
        virtualBottomIndex = 1;

        connected = new WeightedQuickUnionUF(gridIndexBounds * gridIndexBounds);
        connectedToTop = new WeightedQuickUnionUF(gridIndexBounds * gridIndexBounds);

        for (int i = 1; i < grid.length; i++) {
            for (int j = 1; j < grid[i].length; j++) {
                grid[i][j] = false;
            }
        }
        for (int col = 1; col <= gridSize; col++) {
            connected.union(virtualTopIndex, xyTo1D(1, col));
            connectedToTop.union(virtualTopIndex, xyTo1D(1, col));
            connected.union(virtualBottomIndex, xyTo1D(gridSize, col));
        }
    }

    private void connect(int x, int y) {
        connected.union(x, y);
        connectedToTop.union(x, y);
    }

    private int xyTo1D(int row, int col) {
        return gridSize * row + col;
    }

    private void validateIndices(int row, int col) {
        if (row >= grid.length || col >= grid[0].length || row <= 0 || col <= 0) {
            throw new IllegalArgumentException(
                    "Illegal indicies row " + row + " and col " + col);
        }
    }

    // opens the site (row, col) if it is not open already
    public void open(int row, int col) {
        validateIndices(row, col);
        if (grid[row][col]) {
            return;
        }
        totalOpen += 1;
        grid[row][col] = true;
        int posAs1D = xyTo1D(row, col);
        if (row + 1 < grid.length && isOpen(row + 1, col)) {
            connect(posAs1D, xyTo1D(row + 1, col));
        }
        if (row - 1 > 0 && isOpen(row - 1, col)) {
            connect(posAs1D, xyTo1D(row - 1, col));
        }
        if (col + 1 < grid[0].length && isOpen(row, col + 1)) {
            connect(posAs1D, xyTo1D(row, col + 1));
        }
        if (col - 1 > 0 && isOpen(row, col - 1)) {
            connect(posAs1D, xyTo1D(row, col - 1));
        }
    }

    // is the site (row, col) open?
    public boolean isOpen(int row, int col) {
        validateIndices(row, col);
        return grid[row][col];
    }

    // is the site (row, col) full?
    public boolean isFull(int row, int col) {
        validateIndices(row, col);
        return grid[row][col] && connectedToTop.find(virtualTopIndex) == connectedToTop
                .find(xyTo1D(row, col));
    }

    // returns the number of open sites
    public int numberOfOpenSites() {
        return totalOpen;
    }

    // does the system percolate?
    public boolean percolates() {
        return connected.find(virtualTopIndex) == connected.find(virtualBottomIndex)
                && numberOfOpenSites() >= 1;
    }

    // test client (optional)
    // public static void main(String[] args) {
    // }
}
