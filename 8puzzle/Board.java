/* *****************************************************************************
 *  Name: Marcus Ulrich
 **************************************************************************** */

import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdOut;

import java.util.Arrays;

public class Board {
    private final int[][] tiles;
    private final int n;
    private int cacheHammingValue = -1;
    private int cacheManhattanValue = -1;
    private final int cacheGoal = -1;

    // create a board from an n-by-n array of tiles,
    // where tiles[row][col] = tile at (row, col)
    public Board(int[][] tiles) {
        if (tiles == null) {
            throw new IllegalArgumentException("No tiles specified");
        }
        n = tiles.length;
        this.tiles = copyTiles(tiles);
    }

    // string representation of this board
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(n + "\n");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                s.append(String.format("%2d ", tiles[i][j]));
            }
            s.append("\n");
        }
        return s.toString();
    }

    // board dimension n
    public int dimension() {
        return n;
    }

    // number of tiles out of place
    public int hamming() {
        if (cacheHammingValue != -1) {
            return cacheHammingValue;
        }
        int outOfPlace = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (tiles[i][j] != i * n + j + 1 && (i != n - 1 || j != n - 1)) {
                    outOfPlace++;
                }
            }
        }
        cacheHammingValue = outOfPlace;
        return outOfPlace;
    }

    // sum of Manhattan distances between tiles and goal
    public int manhattan() {
        if (cacheManhattanValue != -1) {
            return cacheManhattanValue;
        }
        int totalDistance = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {

                int tileValue = tiles[i][j];
                if (tileValue == 0) {
                    continue;
                }
                int goalHorizontal = (tileValue - 1) % n;
                int goalVertical = Math.floorDiv((tileValue - 1), n);
                int tileHorizontalDiff = Math.abs(goalHorizontal - j);
                int tileVerticalDiff = Math.abs(goalVertical - i);
                totalDistance += tileHorizontalDiff + tileVerticalDiff;
            }
        }
        cacheManhattanValue = totalDistance;
        return totalDistance;
    }

    private int goalCached() {
        if (cacheGoal != -1) {
            return cacheGoal;
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (tiles[i][j] != i * n + j + 1 && !(i == n - 1 && j == n - 1
                        && tiles[i][j] == 0)) {
                    return 0;
                }
            }
        }
        return 1;
    }

    // is this board the goal board?
    public boolean isGoal() {
        return goalCached() == 1;
    }

    // does this board equal y?
    public boolean equals(Object y) {
        if (this == y) {
            return true;
        }
        if (y == null) {
            return false;
        }
        if (this.getClass() != y.getClass()) {
            return false;
        }
        Board that = (Board) y;
        return Arrays.deepEquals(this.tiles, that.tiles);
    }

    private int[] findSquare(int square) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (tiles[i][j] == square) {
                    return new int[] { i, j };
                }
            }
        }
        return new int[] { -1, -1 };
    }

    private int[][] copyTiles(int[][] tilesToCopy) {
        int[][] copy = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                copy[i][j] = tilesToCopy[i][j];
            }
        }
        return copy;
    }

    private int[][] swapSquares(int[][] tilesToSwap, int[] squareA, int[] squareB) {
        int[][] swapped = copyTiles(tilesToSwap);
        int tempValue = swapped[squareA[0]][squareA[1]];
        swapped[squareA[0]][squareA[1]] = swapped[squareB[0]][squareB[1]];
        swapped[squareB[0]][squareB[1]] = tempValue;
        return swapped;
    }

    // all neighboring boards
    public Iterable<Board> neighbors() {
        Stack<Board> neighborBoards = new Stack<Board>();
        int[] position = findSquare(0);
        if (position[0] - 1 >= 0) {
            neighborBoards.push(new Board(
                    swapSquares(tiles, position, new int[] { position[0] - 1, position[1] })));
        }
        if (position[1] - 1 >= 0) {
            neighborBoards.push(new Board(
                    swapSquares(tiles, position, new int[] { position[0], position[1] - 1 })));
        }
        if (position[0] + 1 < n) {
            neighborBoards.push(new Board(
                    swapSquares(tiles, position, new int[] { position[0] + 1, position[1] })));
        }
        if (position[1] + 1 < n) {
            neighborBoards.push(new Board(
                    swapSquares(tiles, position, new int[] { position[0], position[1] + 1 })));
        }
        return neighborBoards;
    }

    // a board that is obtained by exchanging any pair of tiles
    public Board twin() {
        if (tiles[0][0] != 0 && tiles[0][1] != 0) {
            return new Board(swapSquares(tiles, new int[] { 0, 0 }, new int[] { 0, 1 }));
        }
        return new Board(swapSquares(tiles, new int[] { 1, 0 }, new int[] { 1, 1 }));
    }

    // unit testing (not graded)
    public static void main(String[] args) {
        Board solved = new Board(
                new int[][] {
                        { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 }, { 13, 14, 15, 0 }
                });
        Board zeroInPlace = new Board(
                new int[][] {
                        { 2, 1, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 }, { 13, 14, 15, 0 }
                });
        Board b813402765 = new Board(new int[][] {
                { 8, 1, 3 }, { 4, 0, 2 }, { 7, 6, 5 }
        });
        Board b813402765Also = new Board(new int[][] {
                { 8, 1, 3 }, { 4, 0, 2 }, { 7, 6, 5 }
        });
        StdOut.println("Hamming for board b813402765 == 5: " + b813402765.hamming());
        StdOut.println("Manhattan for board b813402765 == 10: " + b813402765.manhattan());
        StdOut.println("Hamming for board solved == 0: " + solved.hamming());
        StdOut.println("Manhattan for board solved == 0: " + solved.manhattan());
        StdOut.println("b813402765 isGoal is false: " + b813402765.isGoal());
        StdOut.println("solved isGoal is true: " + solved.isGoal());
        StdOut.println("zeroInPlace isGoal is false: " + zeroInPlace.isGoal());
        StdOut.println("b813402765 == solved is false: " + b813402765.equals(solved));
        StdOut.println(
                "b813402765 == b813402765Also is true: " + b813402765.equals(b813402765Also));
        StdOut.println("Should print solved and its twin:");
        StdOut.print(b813402765);
        StdOut.print(b813402765.twin());
        StdOut.println("Should print solved and its twin:");
        StdOut.print(solved);
        StdOut.print(solved.twin());
        StdOut.println("Should print b813402765 followed by its neighbors:");
        StdOut.print(b813402765);
        for (Board board : b813402765.neighbors()) {
            StdOut.print(board);
        }
    }

}
