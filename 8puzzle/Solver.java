/* *****************************************************************************
 *  Name: Marcus Ulrich
 **************************************************************************** */

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdOut;

public class Solver {
    private boolean solvable;

    private final Stack<Board> solutionPath = new Stack<Board>();

    // find a solution to the initial board (using the A* algorithm)
    public Solver(Board initial) {
        if (initial == null) {
            throw new IllegalArgumentException("No board specified");
        }
        solve(initial);
    }

    private void solve(Board initial) {
        MinPQ<SearchNode> pq = new MinPQ<Solver.SearchNode>();
        pq.insert(new Solver.SearchNode(initial, null, 0));
        Solver.SearchNode node;

        MinPQ<SearchNode> pqTwin = new MinPQ<Solver.SearchNode>();
        pqTwin.insert(new Solver.SearchNode(initial.twin(), null, 0));
        Solver.SearchNode nodeTwin;

        while (!pq.isEmpty() && !pqTwin.isEmpty()) {
            node = pq.delMin();
            nodeTwin = pqTwin.delMin();
            if (nodeTwin.board.isGoal()) {
                solvable = false;
                return;
            }
            if (node.board.isGoal()) {
                solvable = true;
                do {
                    solutionPath.push(node.board);
                    node = node.previous;
                } while (node != null);
                return;
            }
            for (Board neighborBoard : node.board.neighbors()) {
                if (node.previous == null || !neighborBoard.equals(node.previous.board)) {
                    pq.insert(new Solver.SearchNode(neighborBoard, node, node.moves + 1));
                }
            }
            for (Board neighborBoardTwin : nodeTwin.board.neighbors()) {
                if (nodeTwin.previous == null || !neighborBoardTwin
                        .equals(nodeTwin.previous.board)) {
                    pqTwin.insert(
                            new Solver.SearchNode(neighborBoardTwin, nodeTwin, nodeTwin.moves + 1));
                }
            }
        }
        return;
    }

    // is the initial board solvable? (see below)
    public boolean isSolvable() {
        return solvable;
    }

    // min number of moves to solve initial board; -1 if unsolvable
    public int moves() {
        return solutionPath.size() - 1;
    }

    // sequence of boards in a shortest solution; null if unsolvable
    public Iterable<Board> solution() {
        if (!isSolvable()) {
            return null;
        }
        return solutionPath;
    }

    private static class SearchNode implements Comparable<SearchNode> {
        private final Board board;
        private final SearchNode previous;
        private final int moves;
        private int manhattanValue = -1;

        public SearchNode(Board board, SearchNode previous, int moves) {
            this.board = board;
            this.previous = previous;
            this.moves = moves;
        }

        public int compareTo(SearchNode that) {
            return Integer.compare(manhattan() + moves, that.manhattan() + that.moves);
        }

        private int manhattan() {
            if (manhattanValue == -1) {
                manhattanValue = board.manhattan();
            }
            return manhattanValue;
        }
    }

    // test client (see below)
    public static void main(String[] args) {
        // create initial board from file
        In in = new In(args[0]);
        int n = in.readInt();
        int[][] tiles = new int[n][n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                tiles[i][j] = in.readInt();
        Board initial = new Board(tiles);

        // solve the puzzle
        Solver solver = new Solver(initial);

        // print solution to standard output
        if (!solver.isSolvable())
            StdOut.println("No solution possible");
        else {
            StdOut.println("Minimum number of moves = " + solver.moves());
            for (Board board : solver.solution())
                StdOut.println(board);
        }
    }

}
