/* *****************************************************************************
 *  Name: Marcus Ulrich
 **************************************************************************** */

import edu.princeton.cs.algs4.Bag;
import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.DirectedCycle;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.ST;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.Topological;

public class WordNet {

    private final Digraph hypernymsGraph;
    private final SAP sap;

    private final ST<Integer, String> synsetsST = new ST<Integer, String>();
    private final ST<String, Bag<Integer>> nounsST = new ST<String, Bag<Integer>>();

    // constructor takes the name of the two input files
    public WordNet(String synsets, String hypernyms) {
        if (synsets == null || hypernyms == null) {
            throw new IllegalArgumentException("null not allowed as argument to constructor");
        }
        In hypernymsFile = new In(hypernyms);
        String[] hypernymsArray = hypernymsFile.readAllLines();
        hypernymsGraph = new Digraph(hypernymsArray.length);

        readSynsets(new In(synsets));
        readHypernyms(hypernymsArray);

        DirectedCycle dc = new DirectedCycle(this.hypernymsGraph);
        if (dc.hasCycle()) {
            throw new IllegalArgumentException("Given graph has a cycle");
        }
        Topological topo = new Topological(this.hypernymsGraph);
        if (!topo.hasOrder()) {
            throw new IllegalArgumentException("Given graph is not a rooted DAG");
        }
        sap = new SAP(hypernymsGraph);
    }


    // returns all WordNet nouns
    public Iterable<String> nouns() {
        return nounsST;
    }


    // is the word a WordNet noun?
    public boolean isNoun(String word) {
        if (word == null) {
            throw new IllegalArgumentException("null not allowed as argument");
        }
        return nounsST.contains(word);
    }


    // distance between nounA and nounB (defined below)
    public int distance(String nounA, String nounB) {
        validateNouns(nounA, nounB);
        return sap.length(nounsST.get(nounA), nounsST.get(nounB));
    }

    // a synset (second field of synsets.txt) that is the common ancestor of nounA and nounB
    // in a shortest ancestral path (defined below)
    public String sap(String nounA, String nounB) {
        validateNouns(nounA, nounB);
        // Bag<String> b = synsetsST.get(sap.ancestor(nounsST.get(nounA), nounsST.get(nounB)));

        return synsetsST.get(sap.ancestor(nounsST.get(nounA), nounsST.get(nounB)));
    }


    private void readSynsets(In synsets) {
        String[] fields;
        String[] synsetFields;
        while (synsets.hasNextLine()) {
            String line = synsets.readLine();
            fields = line.split(",");
            int key = Integer.parseInt(fields[0]);
            String synset = fields[1];
            synsetsST.put(key, synset);
            synsetFields = synset.split(" ");
            for (int i = 0; i < synsetFields.length; i++) {
                if (nounsST.contains(synsetFields[i])) {
                    Bag<Integer> b = nounsST.get(synsetFields[i]);
                    b.add(key);
                }
                else {
                    Bag<Integer> b = new Bag<Integer>();
                    b.add(key);
                    nounsST.put(synsetFields[i], b);
                }
            }
        }
    }

    private void readHypernyms(String[] hypernyms) {
        String[] fields;
        for (String line : hypernyms) {
            fields = line.split(",");
            int v = Integer.parseInt(fields[0]);
            for (int i = 1; i < fields.length; i++) {
                hypernymsGraph.addEdge(v, Integer.parseInt(fields[i]));
            }
        }
    }

    private void validateNouns(String nounA, String nounB) {
        if (nounA == null || nounB == null) {
            throw new IllegalArgumentException("null not allowed as argument");
        }
        // Any of the noun arguments in distance() or sap() is not a WordNet noun.
        if (!isNoun(nounA)) {
            throw new IllegalArgumentException(nounA + " is not a WordNet noun");
        }
        if (!isNoun(nounB)) {
            throw new IllegalArgumentException(nounB + " is not a WordNet noun");
        }
    }


    // do unit testing of this class
    public static void main(String[] args) {

        WordNet G = new WordNet("synsets.txt", "hypernyms.txt");
        StdOut.println((G.distance("Black_Plague", "black_marlin") == 33)
                               + ": Black_Plague and black_marlin have distance 33");
        StdOut.println(G.distance("Black_Plague", "black_marlin"));
        StdOut.println();

        StdOut.println(
                (G.sap("worm", "bird").equals("animal animate_being beast brute creature fauna"))
                        + ": worm and bird have a shortest commomn ancestor of \"animal animate_being beast brute creature fauna\"");
        StdOut.println(G.sap("worm", "bird"));
        StdOut.println();

        StdOut.println((G.sap("individual", "edible_fruit").equals("physical_entity"))
                               + ": individual and edible_fruit have a shortest common ancestor of \"physical_entity\"");
        StdOut.println(G.sap("individual", "edible_fruit"));
        StdOut.println();

        // WordNet Gin = new WordNet(args[0], args[1]);

    }
}
