/* *****************************************************************************
 *  Name: Marcus Ulrich
 **************************************************************************** */

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

public class Outcast {
    private final WordNet wordnet;

    public Outcast(WordNet wordnet) { // constructor takes a WordNet object
        this.wordnet = wordnet;
    }

    public String outcast(String[] nouns) { // given an array of WordNet nouns, return an outcast
        String outcast = "";
        int maxDistance = 0;

        for (int i = 0; i < nouns.length; i++) {
            int distance = 0;
            String currentNoun = nouns[i];
            for (int j = 0; j < nouns.length; j++) {
                if (i == j) {
                    continue;
                }
                distance += wordnet.distance(currentNoun, nouns[j]);
            }
            if (distance > maxDistance) {
                outcast = currentNoun;
                maxDistance = distance;
            }
        }
        return outcast;
    }

    public static void main(String[] args) {
        WordNet wordnet = new WordNet(args[0], args[1]);
        Outcast outcast = new Outcast(wordnet);
        for (int t = 2; t < args.length; t++) {
            In in = new In(args[t]);
            String[] nouns = in.readAllStrings();
            StdOut.println(args[t] + ": " + outcast.outcast(nouns));
        }
        WordNet w1 = new WordNet("synsets.txt", "hypernyms.txt");
        Outcast o1 = new Outcast(w1);

        In in5 = new In("outcast5.txt");
        String[] nouns5 = in5.readAllStrings();
        StdOut.println("outcast5.txt should have outcast \"table\": " + o1.outcast(nouns5)
                                                                          .equals("table"));
        StdOut.println("outcast5.txt: " + outcast.outcast(nouns5));
        StdOut.println();

        In in8 = new In("outcast8.txt");
        String[] nouns8 = in8.readAllStrings();
        StdOut.println("outcast8.txt should have outcast \"bed\": " + o1.outcast(nouns8)
                                                                        .equals("bed"));
        StdOut.println("outcast8.txt: " + outcast.outcast(nouns8));
        StdOut.println();

        In in11 = new In("outcast11.txt");
        String[] nouns11 = in11.readAllStrings();
        StdOut.println("outcast11.txt should have outcast \"potato\": " + o1.outcast(nouns11)
                                                                            .equals("potato"));
        StdOut.println("outcast11.txt: " + outcast.outcast(nouns11));
        StdOut.println();

    }

}
