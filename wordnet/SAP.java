/* *****************************************************************************
 *  Name: Marcus Ulrich
 **************************************************************************** */

import edu.princeton.cs.algs4.BreadthFirstDirectedPaths;
import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.SET;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

public class SAP {
    private static final int INFINITY = Integer.MAX_VALUE;
    private final Digraph graph;

    // constructor takes a digraph (not necessarily a DAG)
    public SAP(Digraph G) {
        if (G == null) {
            throw new IllegalArgumentException("null not allowed as argument");
        }
        this.graph = new Digraph(G);
    }

    // length of shortest ancestral path between v and w; -1 if no such path
    // should throw an IllegalArgumentException unless {@code 0 <= v < V}
    public int length(int v, int w) {
        return shortestAncestor(v, w, true);
    }

    // a common ancestor of v and w that participates in a shortest ancestral path; -1 if no such path
    // should throw an IllegalArgumentException unless {@code 0 <= v < V}
    public int ancestor(int v, int w) {
        return shortestAncestor(v, w, false);
    }

    // length of shortest ancestral path between any vertex in v and any vertex in w; -1 if no such path
    public int length(Iterable<Integer> vs, Iterable<Integer> ws) {
        if (vs == null || ws == null) {
            throw new IllegalArgumentException("null not allowed as argument");
        }
        // Any iterable argument contains a null item
        return shortestAncestor(vs, ws, true);
    }

    // a common ancestor that participates in shortest ancestral path; -1 if no such path
    public int ancestor(Iterable<Integer> vs, Iterable<Integer> ws) {
        if (vs == null || ws == null) {
            throw new IllegalArgumentException("null not allowed as argument");
        }
        // Any iterable argument contains a null item
        return shortestAncestor(vs, ws, false);
    }

    // BFS from single source
    private int shortestAncestor(int v, int w, boolean getDistance) {
        if (v == w) {
            return getDistance ? 0 : v;
        }
        Queue<Integer> q = new Queue<Integer>();

        BreadthFirstDirectedPaths bfsV = new BreadthFirstDirectedPaths(graph, v);
        BreadthFirstDirectedPaths bfsW = new BreadthFirstDirectedPaths(graph, w);
        boolean[] marked = new boolean[graph.V()];
        int shortestAncestor = -1;
        int shortestDistance = INFINITY;
        q.enqueue(v);
        q.enqueue(w);
        while (!q.isEmpty()) {
            int sourceVertex = q.dequeue();
            for (int checkVertex :
                    graph.adj(sourceVertex)) {
                if (!marked[checkVertex]) {
                    if (bfsV.hasPathTo(checkVertex) && bfsW.hasPathTo(checkVertex)
                            && bfsV.distTo(checkVertex) + bfsW.distTo(checkVertex)
                            < shortestDistance) {
                        shortestAncestor = checkVertex;
                        shortestDistance = bfsV.distTo(checkVertex) + bfsW.distTo(checkVertex);
                    }
                    marked[checkVertex] = true;
                    q.enqueue(checkVertex);
                }
            }
        }
        if (shortestDistance == INFINITY) {
            shortestDistance = -1;
        }
        return getDistance ? shortestDistance : shortestAncestor;
    }

    // BFS from multiple sources
    private int shortestAncestor(Iterable<Integer> vs, Iterable<Integer> ws, boolean getDistance) {
        Queue<Integer> q = new Queue<Integer>();
        SET<Integer> vals = new SET<Integer>();
        for (Integer s : vs) {
            if (s == null) {
                throw new IllegalArgumentException("null not allowed as argument");
            }
            q.enqueue(s);
            vals.add(s);
        }
        int vsSize = q.size();
        for (Integer s : ws) {
            if (s == null) {
                throw new IllegalArgumentException("null not allowed as argument");
            }
            q.enqueue(s);
            if (vals.contains(s)) {
                return getDistance ? 0 : s;
            }
        }
        if (vsSize == 0 || q.size() - vsSize == 0) {
            return -1;
        }
        boolean[] marked = new boolean[graph.V()];
        int shortestAncestor = -1;
        int shortestDistance = INFINITY;
        BreadthFirstDirectedPaths bfsVs = new BreadthFirstDirectedPaths(graph, vs);
        BreadthFirstDirectedPaths bfsWs = new BreadthFirstDirectedPaths(graph, ws);
        while (!q.isEmpty()) {
            int sourceVertex = q.dequeue();
            for (int checkVertex : graph.adj(sourceVertex)) {
                if (!marked[checkVertex]) {
                    if (bfsVs.hasPathTo(checkVertex) && bfsWs.hasPathTo(checkVertex)
                            && bfsVs.distTo(checkVertex) + bfsWs.distTo(checkVertex)
                            < shortestDistance) {
                        shortestAncestor = checkVertex;
                        shortestDistance = bfsVs.distTo(checkVertex) + bfsWs.distTo(checkVertex);
                    }
                    marked[checkVertex] = true;
                    q.enqueue(checkVertex);
                }
            }
        }
        if (shortestDistance == INFINITY) {
            shortestDistance = -1;
        }
        return getDistance ? shortestDistance : shortestAncestor;
    }


    public static void main(String[] args) {
        // In in1 = new In("digraph-wordnet.txt");
        // Digraph G1 = new Digraph(in1);
        // SAP sap1 = new SAP(G1);
        // Integer a1[] = { 0, 7, 9, 12 };
        // // Bag<Integer> b1 = new Bag<Integer>();
        // // b1.add(13);
        // // b1.add(23);
        // // b1.add(24);
        //
        // Bag<Integer> b2 = new Bag<Integer>();
        // b2.add(6);
        // b2.add(16);
        // b2.add(17);
        //
        // int length = sap1.length(a1, null);
        // int ancestor = sap1.ancestor(a1, b2);
        // StdOut.printf("length = %d, ancestor = %d\n", length, ancestor);
        //
        In in = new In(args[0]);
        Digraph G = new Digraph(in);
        SAP sap = new SAP(G);
        while (!StdIn.isEmpty()) {
            int v = StdIn.readInt();
            int w = StdIn.readInt();
            int length = sap.length(v, w);
            int ancestor = sap.ancestor(v, w);
            StdOut.printf("length = %d, ancestor = %d\n", length, ancestor);
        }
    }
}
